/*async function signOut() {
	await AutomationPracticeHome_Page.clickOnSignOutLink();    
}*/

Feature('ATP - verify user should be log out on clicking on logout page');

Scenario('AJ - This scenario verifies on clicking on SignOut Link user should lands on signIn page',async function (I, AutomationPracticeHome_Page) {

	I.click(AutomationPracticeHome_Page.fields.signOut);	    
    
    let url = await I.grabCurrentUrl();
    
    console.log("I am on SignIn page: " + url);
    
    I.seeInCurrentUrl('controller=authentication&back=my-account');   

});
