/*async function landingPage() {
 await browser.navigateTo(config.baseUrl);
 await browser.pause(3000);
}*/

Feature('ATP - verify user lands to landing page');

var domain = config.baseUrl;

Scenario('AJ - This scenario verifies user should lands on landing page',async function (I) {

    I.amOnPage(domain);
    
    I.wait(3);
    
    let url = await I.grabCurrentUrl();
    
    console.log("I am on landing page: " + url);
    
    I.seeInCurrentUrl('http://automationpractice.com/index.php');   
    
});

