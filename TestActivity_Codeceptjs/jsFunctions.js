const config = {
    baseUrl: "http://www.automationpractice.com"
}

//Automation Practice Home Page Started

class AutomationPracticeHome_PageObject {

'use strict';

let I;

module.exports = {

    _init() {
        I = actor();
},

// insert your locators and methods here

    fields: {

        signIn: "//a[@class='login']",
 		signOut: "//a[@class='logout']",
        userName: "//input[@id='email']",
        password: "//input[@id='passwd']",
        submitLoginBtn:"//button[@id='SubmitLogin']" 
    } 
    
   async loginWithRegisteredUser(email,password){
    if(email){
      //await (this.setEmailId(email));
    	I.fillField((await (this.fields.userName),email);  
      	
    }
    if (password) {
      //await (this.setPassword(password));
    	I.fillField((await (this.fields.password),password);  
    }
     I.click((await (this.fields.submitLoginBtn)));

  } 

const AutomationPracticeHome_Page = new AutomationPracticeHome_PageObject

//Automation Practice Home Page Ended
