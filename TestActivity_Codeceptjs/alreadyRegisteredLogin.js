/*async function alreadyRegisteredLogin() {
  	let loggedIn = false;
    
    while(!loggedIn){
        let userName = await requestData('userName');
        let passWord = await requestData('password');
        await AutomationPracticeHome_Page.loginWithRegisteredUser(userName,passWord);
        if((userName == 'Ajaylunia2011@gmail.com')&&(passWord == '456789')){
            await runCommand('set_enabled','myAccount, True');
            await AutomationPracticeHome_Page.confirmSuccessfulUserAccountCreation('Ajay','Lunia');
            loggedIn = true;
        }
        else if((userName == 'Sean@you-spam.com')&&(passWord == '123456')){
            await runCommand('set_enabled','myAccount, True');
            await AutomationPracticeHome_Page.confirmSuccessfulUserAccountCreation('Sean','Redmond');    
        	loggedIn = true;
        }
        else if((userName == 'ross@you-spam.com')&&(passWord == '123456')){
            await runCommand('set_enabled','myAccount, True');
            await AutomationPracticeHome_Page.confirmSuccessfulUserAccountCreation('Ross','Addinall');    
        	loggedIn = true;
        } 
        else {
            await createVitaqLogEntry("username/password combination has not been accepted");
            await AutomationPracticeHome_Page.confirmUnsuccessfulAccountCreation(userName);
        }
        await recordCoverage("",'password');
        await recordCoverage("",'userName');
   }
} */
Feature('ATP - verify user should be able to login with correct credentials');

Scenario('AJ - This scenario verifies on entering correct username and password user should successfully logged in',async function (I, AutomationPracticeHome_Page) {

    let loggedIn = false;
    
    while(!loggedIn){
        let userName = await requestData('userName');
        let passWord = await requestData('password');
        await AutomationPracticeHome_Page.loginWithRegisteredUser(userName,passWord);
    
        if((userName == 'Ajaylunia2011@gmail.com')&&(passWord == '456789')){
            loggedIn = true;
        }
          else if((userName == 'Sean@you-spam.com')&&(passWord == '123456')){
           	loggedIn = true;
        }
        else if((userName == 'ross@you-spam.com')&&(passWord == '123456')){         
        	loggedIn = true;
        } 
        else {
            await createVitaqLogEntry("username/password combination has not been accepted");
        }
        await recordCoverage("",'password');
        await recordCoverage("",'userName');
   }
});       
