/*async function signIn() {
	await AutomationPracticeHome_Page.clickOnSignInLink();
}*/

Feature('ATP - verify user should lands on SignIn page');

Scenario('AJ - This scenario verifies on clicking on SignIn Link user should lands on signIn page',async function (I, AutomationPracticeHome_Page) {

	I.click(AutomationPracticeHome_Page.fields.signIn);	    
    
    let url = await I.grabCurrentUrl();
    
    console.log("I am on SignIn page: " + url);
    
    I.seeInCurrentUrl('controller=authentication&back=my-account');   

});
