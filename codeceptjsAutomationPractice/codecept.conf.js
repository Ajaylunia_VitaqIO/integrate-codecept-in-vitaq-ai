const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    WebDriverIO: {
      url: 'http://localhost',
      browser: 'chrome'
    }
  },
  include: {
    "home": "./pages/home.js"
  },
  bootstrap: null,
  mocha: {},
  name: 'codeceptjsAutomationPractice',
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}