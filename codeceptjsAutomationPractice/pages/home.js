'use strict';

let I;

module.exports = {

    _init() {
        I = actor();
    },

    // insert your locators and methods here

        fields: {
            signIn: "//a[@class='login']",
            signOut: "//a[@class='logout']",
            userName: "//input[@id='email']",
            password: "//input[@id='passwd']",
            submitLoginBtn:"//button[@id='SubmitLogin']" 
        }, 

        loginWithRegisteredUser(email,password){
        if(email){
            I.waitForVisible( this.fields.userName);
            I.fillField(this.fields.userName,email);  
        }
        if(password){
            I.waitForVisible(this.fields.userName);
            I.fillField(this.fields.password,password);  
        }
        I.click(this.fields.submitLoginBtn);
    } 
} 