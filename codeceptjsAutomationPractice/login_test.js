Feature('login to AutomationPractice.com');

Scenario('test something', async(I,home) => {
    I.amOnPage('http://automationpractice.com/index.php');
    I.see('WOMEN');
    I.click(home.fields.signIn);	
    I.scrollIntoView('.page-heading');
    I.seeInCurrentUrl('controller=authentication&back=my-account');
    home.loginWithRegisteredUser('Ajaylunia2011@gmail.com','456789');  
});
